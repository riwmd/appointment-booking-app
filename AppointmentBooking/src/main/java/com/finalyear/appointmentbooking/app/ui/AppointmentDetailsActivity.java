package com.finalyear.appointmentbooking.app.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.finalyear.appointmentbooking.app.ApplicationConstants;
import com.finalyear.appointmentbooking.app.R;
import com.finalyear.appointmentbooking.app.data.Appointment;
import com.parse.DeleteCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @since 21/04/2014
 */
public class AppointmentDetailsActivity extends Activity
{
    private TextView tvDate;
    private TextView tvStartTime;
    private TextView tvFinishTime;
    private TextView tvDoctor;
    private RelativeLayout tvDoctorAttending;
    private TextView tvRoom;
    private TextView tvBookingDate;
    private Appointment appointment;
    private boolean isPastAppointment = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment_info);

        if (getActionBar() != null)
        {
            getActionBar().setDisplayHomeAsUpEnabled(true);
            getActionBar().setTitle("Appointment Details");
        }

        tvDate = (TextView) findViewById(R.id.appointment_details_date);
        tvStartTime = (TextView) findViewById(R.id.appointment_details_start_time);
        tvFinishTime = (TextView) findViewById(R.id.appointment_details_finish_time);
        tvDoctor = (TextView) findViewById(R.id.appointment_details_doctor);
        tvDoctorAttending = (RelativeLayout) findViewById(R.id.booking_doctor_attending);
        tvDoctorAttending.setClickable(true);
        tvDoctorAttending.setFocusable(true);
        tvRoom = (TextView) findViewById(R.id.appointment_details_room);
        tvBookingDate = (TextView) findViewById(R.id.appointment_details_booking_date);

        tvDoctorAttending.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i = new Intent(AppointmentDetailsActivity.this, DoctorDetailsActivity.class);
                i.putExtra("doctor", appointment.getDoctor().getObjectId());
                startActivity(i);
            }
        });

        if (getIntent() != null)
        {
            setTextFields(getIntent().getStringExtra("appointment"));
            isPastAppointment = getIntent().getBooleanExtra("isPast", false);
        } else if (savedInstanceState != null)
        {
            setTextFields(savedInstanceState.getString("appointment"));
            isPastAppointment = getIntent().getBooleanExtra("isPast", false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {

        super.onCreateOptionsMenu(menu);
        menu.clear();
        if (!isPastAppointment)
        {
            getMenuInflater().inflate(R.menu.appointment_details, menu);
        }
        else
        {
            getMenuInflater().inflate(R.menu.main, menu);
        }
        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.action_cancel:
                cancelAppointment();
        }
        return super.onMenuItemSelected(featureId, item);
    }

    public void cancelAppointment()
    {
        if (ApplicationConstants.isNetworkConnected(this))
        {
            new AlertDialog.Builder(this).setTitle("Delete Appointment")
                    .setMessage("Are you sure you would like to delete this appointment?")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {

                            final ProgressDialog signUpDialog = new ProgressDialog(AppointmentDetailsActivity.this);
                            signUpDialog.setIndeterminate(true);
                            signUpDialog.setMessage("Cancelling appointment. Please wait.");
                            signUpDialog.show();
                            appointment.deleteInBackground(new DeleteCallback()
                            {
                                @Override
                                public void done(ParseException e)
                                {
                                    signUpDialog.dismiss();
                                    if (e == null)
                                    {
                                        Toast.makeText(AppointmentDetailsActivity.this, getString(R.string.mess_appointment_cancelled), Toast.LENGTH_SHORT).show();
                                        NavUtils.navigateUpFromSameTask(AppointmentDetailsActivity.this);
                                    } else
                                    {
                                        Toast.makeText(AppointmentDetailsActivity.this, getString(R.string.error_appointment_cancel), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }
                    })
                    .setNegativeButton(android.R.string.no, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
        else
        {
            Toast.makeText(this, "Please connect to the internet to cancel your appointment", Toast.LENGTH_SHORT).show();
        }
    }

    public void setTextFields(String objectId)
    {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setMessage("Loading details...");
        dialog.setCancelable(false);
        dialog.show();

        ParseQuery<Appointment> query = ParseQuery.getQuery("Appointment");
        query.setCachePolicy(ParseQuery.CachePolicy.NETWORK_ELSE_CACHE);
        query.include("doctor");
        query.include("slot");
        query.getInBackground(objectId, new GetCallback<Appointment>()
        {
            @Override
            public void done(Appointment appointment, ParseException e)
            {
                dialog.dismiss();
                if (e == null)
                {
                    SimpleDateFormat dateFormat = new SimpleDateFormat(ApplicationConstants.DEFAULT_DATE);
                    SimpleDateFormat hourMinute = new SimpleDateFormat(ApplicationConstants.DEFAULT_TIME_12HR);
                    Date startTime = new Date(appointment.getSlot().getStartTime());
                    Date finishTime = new Date(appointment.getSlot().getFinishTime());
                    AppointmentDetailsActivity.this.appointment = appointment;

                    tvDate.setText(dateFormat.format(appointment.getAppointmentDate()));
                    tvStartTime.setText(hourMinute.format(startTime));
                    tvFinishTime.setText(hourMinute.format(finishTime));
                    tvDoctor.setText(appointment.getDoctor().toString());
                    tvRoom.setText(appointment.getDoctor().getOffice());
                    tvBookingDate.setText(dateFormat.format(appointment.getCreatedAt()));
                } else
                {
                    Toast.makeText(AppointmentDetailsActivity.this, "Appointment details unavailable", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putSerializable("appointment", this.appointment);
        outState.putSerializable("isPast", isPastAppointment);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);
        appointment = (Appointment) savedInstanceState.get("appointment");
        isPastAppointment = savedInstanceState.getBoolean("isPast", false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
