package com.finalyear.appointmentbooking.app.ui.interfaces;

/**
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @since 25/04/2014
 */
public interface IClickListener
{
    public void nextFragment();

    public void previousFragment();
}
