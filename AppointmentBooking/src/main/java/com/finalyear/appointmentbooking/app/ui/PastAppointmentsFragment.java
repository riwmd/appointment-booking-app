package com.finalyear.appointmentbooking.app.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.*;
import android.widget.AdapterView;
import android.widget.Toast;
import com.finalyear.appointmentbooking.app.ApplicationConstants;
import com.finalyear.appointmentbooking.app.R;
import com.finalyear.appointmentbooking.app.data.Appointment;
import com.finalyear.appointmentbooking.app.ui.adapter.PastAppointmentAdapter;
import com.parse.ParseQueryAdapter;

/**
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @since 21/04/2014
 */
public class PastAppointmentsFragment extends ListFragment implements AdapterView.OnItemClickListener
{
    private static final int CONTEXT_MENU_VIEW_APP = 1;
    private static final int REQUEST_CODE_BOOK_APPOINTMENT = 100;
    private ParseQueryAdapter<Appointment> adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        setHasOptionsMenu(true);
        adapter = new PastAppointmentAdapter(getActivity());
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onStart()
    {
        super.onStart();
        setEmptyText("No Past Appointments Available. Please Access The Internet For An Updated List");
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Appointment");
        menu.add(Menu.NONE, CONTEXT_MENU_VIEW_APP, 0, "View Appointment");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        final AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        switch (item.getItemId())
        {
            case CONTEXT_MENU_VIEW_APP:
                break;
        }

        return super.onContextItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.view_appointments, menu);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        registerForContextMenu(getListView());
        getListView().setOnItemClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.book_appointment:
                if (ApplicationConstants.isNetworkConnected(getActivity()))
                {
                    Intent i = new Intent(getActivity(), BookingActivity.class);
                    startActivityForResult(i, REQUEST_CODE_BOOK_APPOINTMENT);
                    return true;
                } else
                {
                    Toast.makeText(getActivity(), "Please connect to the internet to book an appointment", Toast.LENGTH_SHORT).show();
                }
            case R.id.refresh_appointments:
                updateAppointmentList();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateAppointmentList()
    {
        adapter.loadObjects();
        setListAdapter(adapter);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        updateAppointmentList();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        // TODO: Show context menu which has options: Book Appointment, Cancel Appointment, View Appointment
        viewAppointmentDetails(adapter.getItem(position));
        // Toast.makeText(getActivity().getBaseContext(), "Item Clicked" + appointments.get(position), Toast.LENGTH_SHORT).show();
    }

    public void viewAppointmentDetails(Appointment appointment)
    {
        Intent intent = new Intent(getActivity(), AppointmentDetailsActivity.class);
        intent.putExtra("appointment", appointment.getObjectId());
        intent.putExtra("isPast", true);
        startActivity(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == REQUEST_CODE_BOOK_APPOINTMENT)
        {
            if (resultCode == getActivity().RESULT_OK)
            {
                updateAppointmentList();
            }
        }
    }
}
