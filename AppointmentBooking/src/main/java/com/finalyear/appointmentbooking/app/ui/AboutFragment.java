package com.finalyear.appointmentbooking.app.ui;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.finalyear.appointmentbooking.app.R;

/**
 * Created by Natasha Whitter on 06/05/2014.
 */
public class AboutFragment extends Fragment
{
    TextView tvVersion;
    TextView tvCreatedBy;
    String version;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_about, container, false);

        tvVersion = (TextView) view.findViewById(R.id.version_num);
        tvCreatedBy = (TextView) view.findViewById(R.id.created_by);

        try
        {
            version = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e)
        {
            version = "1.0";
        }

        tvVersion.setText("Version: " + version);
        tvCreatedBy.setText("Created By: " + "Natasha Whitter");

        return view;
    }
}
