package com.finalyear.appointmentbooking.app.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.finalyear.appointmentbooking.app.R;
import com.finalyear.appointmentbooking.app.data.Doctor;
import com.finalyear.appointmentbooking.app.data.Practice;
import com.finalyear.appointmentbooking.app.data.User;
import com.parse.ParseQuery;
import com.parse.ParseQuery.CachePolicy;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;

/**
 * Created by BM on 09/04/2014.
 */
public class DoctorsAdapter extends ParseQueryAdapter<Doctor>
{
    public DoctorsAdapter(Context context)
    {
        super(context, new ParseQueryAdapter.QueryFactory<Doctor>()
        {
            public ParseQuery<Doctor> create()
            {
                ParseQuery<Practice> query = ParseQuery.getQuery("Practice");
                query.setCachePolicy(CachePolicy.NETWORK_ELSE_CACHE);
                User a = (User) ParseUser.getCurrentUser();
                query.whereEqualTo("objectId", a.getPractice().getObjectId());

                ParseQuery<Doctor> nextQuery = ParseQuery.getQuery("Doctor");
                nextQuery.setCachePolicy(CachePolicy.NETWORK_ELSE_CACHE);
                nextQuery.whereMatchesQuery("practice", query);
                nextQuery.addAscendingOrder("surname");

                return nextQuery;
            }
        });
    }

    @Override
    public View getItemView(final Doctor object, View v, ViewGroup parent)
    {
        // TODO Auto-generated method stub

        if (v == null)
        {
            v = View.inflate(getContext(), R.layout.item_list_doctors, null);
        }

        super.getItemView(object, v, parent);

        TextView tvDoctor = (TextView) v.findViewById(R.id.doctor_name);
        TextView tvRoom = (TextView) v.findViewById(R.id.doctor_room);

        tvDoctor.setText(object.toString());
        tvRoom.setText(object.getOffice());

        return v;
    }

}