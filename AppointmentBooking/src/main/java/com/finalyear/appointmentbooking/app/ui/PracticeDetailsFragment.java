package com.finalyear.appointmentbooking.app.ui;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.finalyear.appointmentbooking.app.R;
import com.finalyear.appointmentbooking.app.data.Practice;
import com.finalyear.appointmentbooking.app.data.User;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

/**
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @since 28/04/2014
 */
public class PracticeDetailsFragment extends Fragment
{
    TextView tvName;
    TextView tvStreet;
    TextView tvCity;
    TextView tvCounty;
    TextView tvPostcode;
    TextView tvTelephone;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.activity_practice_info, container, false);

        tvName = (TextView) view.findViewById(R.id.practice_name);
        tvStreet = (TextView) view.findViewById(R.id.practice_street);
        tvCity = (TextView) view.findViewById(R.id.practice_city);
        tvCounty = (TextView) view.findViewById(R.id.practice_county);
        tvPostcode = (TextView) view.findViewById(R.id.practice_postcode);
        tvTelephone = (TextView) view.findViewById(R.id.practice_telephone);

        setTextFields();

        return view;
    }

    @Override
    public void onResume()
    {
        super.onResume();
        setTextFields();
    }

    public void setTextFields()
    {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setIndeterminate(true);
        dialog.setMessage("Loading practice details...");
        dialog.setCancelable(false);
        dialog.show();

        User user = (User) User.getCurrentUser();

        Log.d("TAG", user.getPractice().getObjectId());

        ParseQuery<Practice> query = ParseQuery.getQuery("Practice");
        query.setCachePolicy(ParseQuery.CachePolicy.NETWORK_ELSE_CACHE);
        query.include("address");
        query.getInBackground(user.getPractice().getObjectId(), new GetCallback<Practice>()
        {
            @Override
            public void done(Practice practice, ParseException e)
            {
                dialog.dismiss();
                if (e == null)
                {
                    tvName.setText(practice.getName());
                    tvStreet.setText(practice.getAddress().getStreet());
                    tvCity.setText(practice.getAddress().getCity());
                    tvCounty.setText(practice.getAddress().getCounty());
                    tvPostcode.setText(practice.getAddress().getPostcode());
                    tvTelephone.setText(practice.getTelephone());
                } else
                {
                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

        });
    }
}
