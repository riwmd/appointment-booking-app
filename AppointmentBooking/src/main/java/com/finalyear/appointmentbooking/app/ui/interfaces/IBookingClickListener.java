package com.finalyear.appointmentbooking.app.ui.interfaces;

import com.finalyear.appointmentbooking.app.data.Appointment;
import com.finalyear.appointmentbooking.app.data.Doctor;

/**
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @since 17/04/2014
 */
public interface IBookingClickListener extends IClickListener
{
    public void onListClick(Doctor doctor);

    public void onListClick(Appointment newBooking);

    public void fragmentResults(int result);
}
