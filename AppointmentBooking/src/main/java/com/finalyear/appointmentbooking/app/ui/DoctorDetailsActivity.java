package com.finalyear.appointmentbooking.app.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;
import com.finalyear.appointmentbooking.app.ApplicationConstants;
import com.finalyear.appointmentbooking.app.R;
import com.finalyear.appointmentbooking.app.data.Doctor;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.text.SimpleDateFormat;

/**
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @since 23/04/2014
 */
public class DoctorDetailsActivity extends Activity
{
    private TextView tvName;
    private TextView tvBirthDate;
    private TextView tvGender;
    private TextView tvOffice;
    private TextView tvJoined;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_info);

        if (getActionBar() != null)
        {
            getActionBar().setDisplayHomeAsUpEnabled(true);
            getActionBar().setTitle("Doctor Details");
        }

        tvName = (TextView) findViewById(R.id.doctor_details_name);
        tvBirthDate = (TextView) findViewById(R.id.doctor_details_birth_date);
        tvGender = (TextView) findViewById(R.id.doctor_details_gender);
        tvOffice = (TextView) findViewById(R.id.doctor_details_office);
        tvJoined = (TextView) findViewById(R.id.doctor_details_joined);

        if (getIntent() != null)
        {
            setTextFields(getIntent().getStringExtra("doctor"));
        } else if (savedInstanceState != null)
        {
            setTextFields(savedInstanceState.getString("doctor"));
        }
    }

    public void setTextFields(String objectId)
    {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setMessage("Loading doctor details...");
        dialog.setCancelable(false);
        dialog.show();

        ParseQuery<Doctor> query = ParseQuery.getQuery("Doctor");
        query.setCachePolicy(ParseQuery.CachePolicy.NETWORK_ELSE_CACHE);
        query.include("practice");
        query.getInBackground(objectId, new GetCallback<Doctor>()
        {
            @Override
            public void done(Doctor doctor, ParseException e)
            {
                if (e == null)
                {
                    dialog.dismiss();
                    SimpleDateFormat dateFormat = new SimpleDateFormat(ApplicationConstants.DEFAULT_DATE);
                    // DoctorDetailsActivity.this.doctor = doctor;

                    tvName.setText(doctor.toString());
                    tvBirthDate.setText(dateFormat.format(doctor.getDateOfBirth()));
                    tvGender.setText(doctor.getGender());
                    tvOffice.setText(doctor.getOffice());
                    tvJoined.setText(dateFormat.format(doctor.getCreatedAt()));
                } else
                {
                    Toast.makeText(DoctorDetailsActivity.this, "Doctor details unavailable", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
