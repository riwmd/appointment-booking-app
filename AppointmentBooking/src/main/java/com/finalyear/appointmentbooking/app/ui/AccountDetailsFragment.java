package com.finalyear.appointmentbooking.app.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.finalyear.appointmentbooking.app.ApplicationConstants;
import com.finalyear.appointmentbooking.app.R;
import com.finalyear.appointmentbooking.app.data.Appointment;
import com.finalyear.appointmentbooking.app.data.Practice;
import com.finalyear.appointmentbooking.app.data.User;
import com.finalyear.appointmentbooking.app.ui.dialog.*;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

/**
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @since 25/04/2014
 */
public class AccountDetailsFragment extends Fragment
{
    private final int REQUEST_FIRST_NAME = 1;
    private final int REQUEST_SURNAME = 2;
    private final int REQUEST_DOB = 3;
    private final int REQUEST_EMAIL = 4;
    private final int REQUEST_PASSWORD = 5;
    private final int REQUEST_PRACTICE = 6;

    View.OnClickListener onDetailClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            if (ApplicationConstants.isNetworkConnected(getActivity()))
            {
                switch (v.getId())
                {
                    case R.id.account_first_name_edit:
                        EditFirstNameDialog firstNameDialog = new EditFirstNameDialog();
                        firstNameDialog.setTargetFragment(AccountDetailsFragment.this, REQUEST_FIRST_NAME);
                        firstNameDialog.show(getFragmentManager(), "firstNameDialog");
                        setTextFields();
                        break;
                    case R.id.account_surname_edit:
                        EditSurnameDialog surnameDialog = new EditSurnameDialog();
                        surnameDialog.setTargetFragment(AccountDetailsFragment.this, REQUEST_SURNAME);
                        surnameDialog.show(getFragmentManager(), "surnameDialog");
                        setTextFields();
                        break;
                    case R.id.account_birth_edit:
                        EditDateOfBirthDialog dateOfBirthDialog = new EditDateOfBirthDialog();
                        dateOfBirthDialog.setTargetFragment(AccountDetailsFragment.this, REQUEST_DOB);
                        dateOfBirthDialog.show(getFragmentManager(), "dateOfBirthDialog");
                        setTextFields();
                        break;
                    case R.id.account_email_edit:
                        EditEmailDialog emailDialog = new EditEmailDialog();
                        emailDialog.setTargetFragment(AccountDetailsFragment.this, REQUEST_EMAIL);
                        emailDialog.show(getFragmentManager(), "emailDialog");
                        setTextFields();
                        break;
                    case R.id.account_password_edit:
                        EditPasswordDialog passwordDialog = new EditPasswordDialog();
                        passwordDialog.setTargetFragment(AccountDetailsFragment.this, REQUEST_PASSWORD);
                        passwordDialog.show(getFragmentManager(), "passwordDialog");
                        setTextFields();
                        break;
                    case R.id.account_practice_edit:
                        EditPracticeDialog practiceDialog = new EditPracticeDialog();
                        practiceDialog.setTargetFragment(AccountDetailsFragment.this, REQUEST_PRACTICE);
                        practiceDialog.show(getFragmentManager(), "practiceDialog");
                        setTextFields();
                        break;
                    case R.id.account_close_edit:
                        showCloseAccountDialog();
                        break;
                }
            } else
            {
                Toast.makeText(getActivity(), "Please connect to the internet to change account details", Toast.LENGTH_SHORT).show();
            }
        }
    };
    private TextView tvFirstName;
    private TextView tvSurname;
    private TextView tvDateOfBirth;
    private TextView tvEmail;
    private TextView tvPractice;
    private RelativeLayout rlFirstName;
    private RelativeLayout rlSurname;
    private RelativeLayout rlDateOfBirth;
    private RelativeLayout rlEmail;
    private RelativeLayout rlPassword;
    private RelativeLayout rlPractice;
    private RelativeLayout rlCloseAccount;

    public void showCloseAccountDialog()
    {
        final AlertDialog.Builder closeDialog = new AlertDialog.Builder(getActivity());
        closeDialog.setTitle("Close Account");
        closeDialog.setMessage("Your account will be deactivated, all booked appointments will be cancelled");
        closeDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {

                dialog.cancel();
            }
        });

        closeDialog.setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                if (ApplicationConstants.isNetworkConnected(getActivity()))
                {
                    // TODO: Remove appointments from database and remove user
                    ParseQuery<Appointment> query = new ParseQuery<Appointment>("Appointment");
                    Calendar today = Calendar.getInstance(TimeZone.getDefault());
                    today.set(Calendar.HOUR_OF_DAY, 0);
                    today.set(Calendar.SECOND, 0);
                    today.set(Calendar.MINUTE, 0);
                    today.set(Calendar.MILLISECOND, 0);
                    query.whereGreaterThanOrEqualTo("appointmentDate", today.getTime());
                    query.findInBackground(new FindCallback<Appointment>()
                    {
                        @Override
                        public void done(List<Appointment> appointments, ParseException e)
                        {
                            if (e == null)
                            {
                                Log.d("AccountDetailsFragment", "Appointment : " + appointments.size());
                                for (Appointment appointment : appointments)
                                {
                                    appointment.deleteInBackground();
                                }
                            }
                            User.getCurrentUser().deleteInBackground();
                            User.logOut();
                        }
                    });

                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else
                {
                    Toast.makeText(getActivity(), "Please connect to the internet to deactivate your account", Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }
        });
        closeDialog.show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.activity_user_details, container, false);

        tvFirstName = (TextView) view.findViewById(R.id.account_first_name);
        tvSurname = (TextView) view.findViewById(R.id.account_surname);
        tvDateOfBirth = (TextView) view.findViewById(R.id.account_birth);
        tvEmail = (TextView) view.findViewById(R.id.account_email);
        tvPractice = (TextView) view.findViewById(R.id.account_practice);

        rlFirstName = (RelativeLayout) view.findViewById(R.id.account_first_name_edit);
        rlSurname = (RelativeLayout) view.findViewById(R.id.account_surname_edit);
        rlDateOfBirth = (RelativeLayout) view.findViewById(R.id.account_birth_edit);
        rlEmail = (RelativeLayout) view.findViewById(R.id.account_email_edit);
        rlPassword = (RelativeLayout) view.findViewById(R.id.account_password_edit);
        rlPractice = (RelativeLayout) view.findViewById(R.id.account_practice_edit);
        rlCloseAccount = (RelativeLayout) view.findViewById(R.id.account_close_edit);

        rlFirstName.setOnClickListener(onDetailClick);
        rlSurname.setOnClickListener(onDetailClick);
        rlDateOfBirth.setOnClickListener(onDetailClick);
        rlEmail.setOnClickListener(onDetailClick);
        rlPassword.setOnClickListener(onDetailClick);
        rlPractice.setOnClickListener(onDetailClick);
        rlCloseAccount.setOnClickListener(onDetailClick);

        setTextFields();

        return view;
    }

    public void setTextFields()
    {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setIndeterminate(true);
        dialog.setMessage("Loading account details...");
        dialog.setCancelable(false);
        dialog.show();

        User user = (User) User.getCurrentUser();
        SimpleDateFormat dateFormat = new SimpleDateFormat(ApplicationConstants.DEFAULT_DATE);
        tvFirstName.setText(user.getFirstName());
        tvSurname.setText(user.getSurname());
        tvDateOfBirth.setText(dateFormat.format(user.getDateOfBirth()));
        tvEmail.setText(user.getUsername());

        user.getPractice().fetchIfNeededInBackground(new GetCallback<Practice>()
        {
            @Override
            public void done(Practice practice, ParseException e)
            {
                if (e == null)
                {
                    dialog.dismiss();
                    tvPractice.setText(practice.getName());
                } else
                {
                    Toast.makeText(getActivity(), "Practice details unavailable", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_DOB || requestCode == REQUEST_EMAIL || requestCode == REQUEST_FIRST_NAME || requestCode == REQUEST_SURNAME || requestCode == REQUEST_PASSWORD || requestCode == REQUEST_PRACTICE)
        {
            if (resultCode == getActivity().RESULT_OK)
            {
                setTextFields();
            }
        }
    }
}
