package com.finalyear.appointmentbooking.app.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import com.finalyear.appointmentbooking.app.data.User;

/**
 * Logs the current user out of the application and shows the login screen
 *
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @since 19/04/2014
 */
public class LogoutFragment extends Fragment
{
    private static final String TAG = "LogoutFragment";

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (User.getCurrentUser() != null)
        {
            Log.d(TAG, "User [" + User.getCurrentUser() + "] has logged out");
            User.logOut();
        }

        Intent intent = new Intent(getActivity(), LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Log.d(TAG, "Starting activity : " + LoginActivity.class.getName());
        startActivity(intent);
        getActivity().finish();
    }
}
