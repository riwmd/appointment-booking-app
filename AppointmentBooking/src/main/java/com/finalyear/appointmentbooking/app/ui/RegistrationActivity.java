package com.finalyear.appointmentbooking.app.ui;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import com.finalyear.appointmentbooking.app.R;
import com.finalyear.appointmentbooking.app.data.Practice;
import com.finalyear.appointmentbooking.app.ui.adapter.RegistrationPagerAdapter;
import com.finalyear.appointmentbooking.app.ui.interfaces.IRegisterClickListener;
import com.finalyear.appointmentbooking.app.view.CustomViewPager;

/**
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @since 25/04/2014
 */
public class RegistrationActivity extends FragmentActivity implements IRegisterClickListener
{
    private CustomViewPager pager;
    private RegistrationPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewpager);

        pager = (CustomViewPager) findViewById(R.id.viewpager);
        if (getActionBar() != null)
        {
            getActionBar().setDisplayHomeAsUpEnabled(true);
            getActionBar().setTitle("Account Registration");
        }

        adapter = new RegistrationPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
    }

    @Override
    public void onBackPressed()
    {
        if (pager.getCurrentItem() == 0)
        {
            super.onBackPressed();
        } else
        {
            pager.setCurrentItem(pager.getCurrentItem() - 1);
        }
    }

    @Override
    public void previousFragment()
    {
        if (pager.getCurrentItem() > 0)
        {
            pager.setCurrentItem(pager.getCurrentItem() - 1);
        }
    }

    @Override
    public void nextFragment()
    {
        if (pager.getCurrentItem() + 1 < adapter.getCount())
        {
            pager.setCurrentItem(pager.getCurrentItem() + 1);
        }
    }

    @Override
    public void onListClick(Practice practice)
    {
        RegistrationAccountFragment object = (RegistrationAccountFragment) getSupportFragmentManager().getFragments().get(1);
        object.setPractice(practice);
    }
}
