package com.finalyear.appointmentbooking.app.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import com.finalyear.appointmentbooking.app.R;
import com.finalyear.appointmentbooking.app.data.User;
import com.finalyear.appointmentbooking.app.nav.NavigationDrawer;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.RefreshCallback;

/**
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @since 03/04/2014
 */
public class SplashScreenActivity extends Activity
{
    private static final String TAG = "SplashScreenActivity";
    private static int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        ParseAnalytics.trackAppOpened(getIntent());

        setContentView(R.layout.activity_splash_screen);

        new Handler().postDelayed(new Runnable()
        {

            @Override
            public void run()
            {
                // If user is login in, then start the appointment activities
                if (User.getCurrentUser() != null)
                {
                    User.getCurrentUser().refreshInBackground(new RefreshCallback()
                    {
                        @Override
                        public void done(ParseObject parseObject, ParseException e)
                        {
                            Log.d(TAG, "ParseUser updated : " + parseObject);
                        }
                    });

                    Intent intent = new Intent(SplashScreenActivity.this, NavigationDrawer.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else
                {
                    // This method will be executed once the timer is over
                    // Start the app's login activity
                    Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }

            }
        }, SPLASH_TIME_OUT);
    }
}
