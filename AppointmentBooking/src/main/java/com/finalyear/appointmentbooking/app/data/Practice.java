package com.finalyear.appointmentbooking.app.data;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.Serializable;

/**
 * This class is an extension of ParseObject to access information about the IGPPracticeRepository
 *
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @see com.parse.ParseObject
 * @since 08/04/2014
 */
@ParseClassName("Practice")
public class Practice extends ParseObject implements Serializable
{
    /**
     * Empty constructor needed for ParseObject class
     */
    public Practice()
    {
    }

    /**
     * Gets the class which the query will use to access Practice data.
     *
     * @return query to fetch Practice objects
     * @see com.parse.ParseQuery
     */
    public static ParseQuery<Practice> getQuery()
    {
        return ParseQuery.getQuery(Practice.class);
    }

    /**
     * Gets the name of this Practice.
     *
     * @return this Practice's name
     */
    public String getName()
    {
        return super.getString("name");
    }

    /**
     * Changes the name of this Practice.
     *
     * @param name This Practice's new name
     */
    public void setName(String name)
    {
        super.put("name", name);
    }

    /**
     * Gets the address assigned to this Practice.
     *
     * @return this Practice's assigned address
     */
    public Address getAddress()
    {
        return (Address) super.getParseObject("address");
    }

    /**
     * Changes the address assigned to this Practice.
     *
     * @param address This Practice's new address
     */
    public void setAddress(Address address)
    {
        super.put("address", address);
    }

    /**
     * Gets the telephone of this Practice
     *
     * @return this Practice's telephone
     */
    public String getTelephone()
    {
        return super.getString("telephone");
    }

    /**
     * Changes the telephone of this Practice.
     *
     * @param telephone This Practice's new telephone
     */
    public void setTelephone(String telephone)
    {
        super.put("telephone", telephone);
    }

    /**
     * Gets the name of this Practice.
     *
     * @return this Practice's name as String
     * @see String#toString()
     */
    @Override
    public String toString()
    {
        return getName();
    }
}
