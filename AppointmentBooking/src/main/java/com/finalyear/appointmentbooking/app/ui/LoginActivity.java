package com.finalyear.appointmentbooking.app.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.finalyear.appointmentbooking.app.ApplicationConstants;
import com.finalyear.appointmentbooking.app.R;
import com.finalyear.appointmentbooking.app.Validation;
import com.finalyear.appointmentbooking.app.nav.NavigationDrawer;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

/**
 * @author Natasha Whitter <Natasha Whitter>
 * @version 2.0
 * @since 19/04/2014
 */
public class LoginActivity extends Activity
{
    private static final String TAG = "LoginSelectorActivity";
    final OnClickListener btnOnClickListener = new OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            final String email = etEmail.getText().toString().toLowerCase().trim();
            final String password = etPassword.getText().toString().trim();
            Intent intent;

            switch (v.getId())
            {
                // This button validates the inputted details and if there correct, sends the information to Parse to login the user
                case R.id.account_login:
                    Log.d(TAG, "Login button clicked : " + v.getId());
                    accountLogin(email, password);
                    break;
                // This button loads the registration activity
                case R.id.account_registration:
                    Log.d(TAG, "Registration button clicked : " + v.getId());
                    intent = new Intent(getApplicationContext(), RegistrationActivity.class);
                    Log.d(TAG, "Starting activity : " + RegistrationActivity.class.getName());
                    startActivity(intent);
                    break;
                // This button loads the password resetting activity
                case R.id.account_password_recovery:
                    Log.d(TAG, "Password recovery button clicked : " + v.getId());
                    intent = new Intent(getApplicationContext(), PasswordRecoveryActivity.class);
                    intent.putExtra("email", email);
                    Log.d(TAG, "Starting activity : " + PasswordRecoveryActivity.class.getName());
                    startActivity(intent);
                    break;
            }
        }
    };
    private BroadcastReceiver receiver;
    private LinearLayout llAccount;
    private Button btnLogin;
    private TextView tvRegistration;
    private TextView tvPasswordReset;
    private EditText etEmail;
    private EditText etPassword;

    /**
     * If the user's information is correct and there's an available internet connection,
     * a dialog is shown while the user is logged into their account. If the account login
     * is successful, then the navigation activity is started, if not, then the ParseException
     * is shown in the toast
     * <p/>
     * TODO: Add validation for internet connection (use the broadcast receiver)
     *
     * @param email    the email entered in etEmail
     * @param password the password entered in etPassword
     */
    public void accountLogin(String email, String password)
    {
        if (ValidationIsCorrect())
        {
            final ProgressDialog signUpDialog = new ProgressDialog(LoginActivity.this);
            signUpDialog.setIndeterminate(true);
            signUpDialog.setMessage("Accessing account. Please wait.");
            signUpDialog.show();

            ParseUser.logInInBackground(email, password, new LogInCallback()
            {
                @Override
                public void done(ParseUser user, ParseException e)
                {
                    signUpDialog.dismiss();

                    if (e == null)
                    {
                        Intent intent = new Intent(LoginActivity.this, NavigationDrawer.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        Log.d(TAG, "Starting activity : " + NavigationDrawer.class.getName());
                        startActivity(intent);
                        finish();
                    } else
                    {
                        Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                        Log.d(TAG, "ParseException : " + e.getMessage());
                    }
                }
            });
        }
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        this.unregisterReceiver(receiver);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        IntentFilter filter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        receiver = new BroadcastReceiver()
        {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                boolean notConnected = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);

                // If connectivity is available, do this, if not then do else
                if (notConnected)
                {
                    llAccount.setVisibility(View.GONE);
                } else
                {
                    llAccount.setVisibility(View.VISIBLE);
                }
            }
        };
        this.registerReceiver(receiver, filter);
    }

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_login);

        llAccount = (LinearLayout) findViewById(R.id.account_extras);
        btnLogin = (Button) findViewById(R.id.account_login);
        tvRegistration = (TextView) findViewById(R.id.account_registration);
        tvPasswordReset = (TextView) findViewById(R.id.account_password_recovery);
        etEmail = (EditText) findViewById(R.id.account_email);
        etPassword = (EditText) findViewById(R.id.account_password);

        btnLogin.setOnClickListener(btnOnClickListener);
        tvRegistration.setOnClickListener(btnOnClickListener);
        tvPasswordReset.setOnClickListener(btnOnClickListener);
    }

    /**
     * Checks if the user's information is correctly formatted. The method returns
     * false if the information is incorrect and shows a toast with information about
     * the validation, returns true if the information is correctly formatted.
     *
     * @return the check if the information is correct
     * @since 20/02/2014
     */
    private boolean ValidationIsCorrect()
    {
        if (!ApplicationConstants.isNetworkConnected(this))
        {
            Log.d(TAG, "No Internet Connection Available");
            Toast.makeText(this, R.string.error_internet_connection, Toast.LENGTH_SHORT).show();
            return false;
        } else if (Validation.isEmpty(etEmail))
        {
            Log.d(TAG, "Email is required");
            Toast.makeText(this, getString(R.string.error_email_required), Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (!Validation.isValidEmail(etEmail))
        {
            Log.d(TAG, "Email is incorrect");
            Toast.makeText(this, R.string.error_invalid_email, Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (Validation.isEmpty(etPassword))
        {
            Toast.makeText(this, getString(R.string.error_password_required), Toast.LENGTH_SHORT).show();
            return false;
        }

        Log.d(TAG, "Validation correct");
        return true;
    }
}
