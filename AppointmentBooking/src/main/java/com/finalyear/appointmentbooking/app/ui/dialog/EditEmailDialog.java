package com.finalyear.appointmentbooking.app.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.finalyear.appointmentbooking.app.R;
import com.finalyear.appointmentbooking.app.Validation;
import com.finalyear.appointmentbooking.app.data.User;
import com.parse.ParseException;
import com.parse.SaveCallback;

/**
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @since 27/04/2014
 */
public class EditEmailDialog extends DialogFragment
{
    private EditText etEmail;
    private User user;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_dialog_edit_name, null);
        etEmail = (EditText) view.findViewById(R.id.name);
        user = (User) User.getCurrentUser();
        etEmail.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

        dialog.setView(view);
        dialog.setTitle("Change Email");
        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int position)
            {
                if (Validation.isValidEmail(etEmail) && !Validation.isEmpty(etEmail))
                {
                    saveEmail();
                    dialog.dismiss();
                } else
                {
                    Toast.makeText(getActivity(), "Incorrect name entered", Toast.LENGTH_SHORT).show();
                }
            }
        });

        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int position)
            {
                getTargetFragment().onActivityResult(getTargetRequestCode(), getActivity().RESULT_CANCELED, null);
                dialog.cancel();
            }
        });

        return dialog.create();
    }

    private void saveEmail()
    {
        user.setSurname(etEmail.getText().toString());
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setIndeterminate(true);
        dialog.setMessage("Changing email. Please wait.");
        dialog.show();
        user.saveInBackground(new SaveCallback()
        {
            @Override
            public void done(ParseException e)
            {
                dialog.dismiss();
                if (e == null)
                {
                    getTargetFragment().onActivityResult(getTargetRequestCode(), getActivity().RESULT_OK, null);
                }
            }
        });
    }
}
