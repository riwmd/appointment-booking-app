package com.finalyear.appointmentbooking.app.nav;

import android.app.ActionBar;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.finalyear.appointmentbooking.app.R;
import com.finalyear.appointmentbooking.app.nav.adapter.FragmentNavAdapter;
import com.finalyear.appointmentbooking.app.nav.data.FragmentNavItem;

import java.util.ArrayList;

/**
 * @author Nathan Esquenazi
 * @see <a href="https://gist.github.com/nesquena/4e9f618b71c30842e89c">FragmentNavigationDrawer.java</a>
 */
public class FragmentNavigationDrawer extends DrawerLayout
{
    private ActionBarDrawerToggle drawerToggle;
    private ListView lvDrawer;
    private FragmentNavAdapter drawerAdapter;
    private ArrayList<FragmentNavItem> drawerNavItems;
    private int drawerContainerRes;

    public FragmentNavigationDrawer(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
    }

    public FragmentNavigationDrawer(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public FragmentNavigationDrawer(Context context)
    {
        super(context);
    }

    public void setupDrawerConfiguration(ListView drawerListView, int drawerContainerRes)
    {
        // Setup navigation items array
        drawerNavItems = new ArrayList<FragmentNavItem>();
        // Set the adapter for the list view
        drawerAdapter = new FragmentNavAdapter(getActivity(), new ArrayList<FragmentNavItem>());
        this.drawerContainerRes = drawerContainerRes;
        // Setup drawer list view and related adapter
        lvDrawer = drawerListView;
        lvDrawer.setAdapter(drawerAdapter);
        // Setup item listener
        lvDrawer.setOnItemClickListener(new FragmentDrawerItemListener());
        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        drawerToggle = setupDrawerToggle();
        setDrawerListener(drawerToggle);
        // Setup action buttons
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
    }

    public void addNavItem(String navTitle, int icon, String windowTitle, Class<? extends Fragment> fragmentClass)
    {
        drawerAdapter.add(new FragmentNavItem(navTitle, icon, fragmentClass));
        drawerNavItems.add(new FragmentNavItem(windowTitle, fragmentClass));
    }

    /**
     * Swaps fragments in the main content view
     */
    public void selectDrawerItem(int position)
    {
        // Create a new fragment and specify the fragment to show based on
        // position
        FragmentNavItem navItem = drawerNavItems.get(position);
        Fragment fragment = null;
        try
        {
            fragment = navItem.getFragmentClass().newInstance();
            Bundle args = navItem.getFragmentArgs();
            if (args != null)
            {
                fragment.setArguments(args);
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(drawerContainerRes, fragment).commit();

        // Highlight the selected item, update the title, and close the drawer
        lvDrawer.setItemChecked(position, true);
        setTitle(navItem.getTitle());
        closeDrawer(lvDrawer);
    }

    public ActionBarDrawerToggle getDrawerToggle()
    {
        return drawerToggle;
    }

    private FragmentActivity getActivity()
    {
        return (FragmentActivity) getContext();
    }

    private ActionBar getActionBar()
    {
        return getActivity().getActionBar();
    }

    private void setTitle(CharSequence title)
    {
        getActionBar().setTitle(title);
    }

    private ActionBarDrawerToggle setupDrawerToggle()
    {
        return new ActionBarDrawerToggle(getActivity(), /* host Activity */
                this, /* DrawerLayout object */
                R.drawable.ic_drawer, /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open, /* "open drawer" description for accessibility */
                R.string.drawer_close /* "close drawer" description for accessibility */
        )
        {
            public void onDrawerClosed(View view)
            {
                // setTitle(getCurrentTitle());
                getActivity().invalidateOptionsMenu(); // call onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView)
            {
                // setTitle("Navigate");
                getActivity().invalidateOptionsMenu(); // call onPrepareOptionsMenu()
            }
        };
    }

    public boolean isDrawerOpen()
    {
        return isDrawerOpen(lvDrawer);
    }

    private class FragmentDrawerItemListener implements ListView.OnItemClickListener
    {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id)
        {
            selectDrawerItem(position);
        }
    }
}