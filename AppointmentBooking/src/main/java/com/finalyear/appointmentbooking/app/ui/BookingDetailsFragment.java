package com.finalyear.appointmentbooking.app.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.*;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.finalyear.appointmentbooking.app.ApplicationConstants;
import com.finalyear.appointmentbooking.app.R;
import com.finalyear.appointmentbooking.app.data.Appointment;
import com.finalyear.appointmentbooking.app.data.User;
import com.finalyear.appointmentbooking.app.ui.interfaces.IBookingClickListener;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.SaveCallback;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @since 19/04/2014
 */
public class BookingDetailsFragment extends Fragment
{
    private RelativeLayout tvDoctorAttending;
    private TextView tvDate;
    private TextView tvStartTime;
    private TextView tvFinishTime;
    private TextView tvDoctor;
    private TextView tvRoom;
    private Appointment appointment;
    private IBookingClickListener listener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.activity_appointment_info, container, false);
        setHasOptionsMenu(true);
        tvDate = (TextView) view.findViewById(R.id.appointment_details_date);
        tvStartTime = (TextView) view.findViewById(R.id.appointment_details_start_time);
        tvFinishTime = (TextView) view.findViewById(R.id.appointment_details_finish_time);
        tvDoctor = (TextView) view.findViewById(R.id.appointment_details_doctor);
        tvRoom = (TextView) view.findViewById(R.id.appointment_details_room);
        view.findViewById(R.id.booking_date_details).setVisibility(View.GONE);
        view.findViewById(R.id.booking_date_line).setVisibility(View.GONE);

        if (savedInstanceState != null)
        {
            appointment = (Appointment) savedInstanceState.getSerializable("appointment");
            setTextFields();
        }

        tvDoctorAttending = (RelativeLayout) view.findViewById(R.id.booking_doctor_attending);
        tvDoctorAttending.setClickable(true);
        tvDoctorAttending.setFocusable(true);

        tvDoctorAttending.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i = new Intent(getActivity(), DoctorDetailsActivity.class);
                i.putExtra("doctor", appointment.getDoctor().getObjectId());
                startActivity(i);
            }
        });


        return view;
    }

    public void setAppointment(final Appointment appointment)
    {
        if (appointment != null)
        {
            this.appointment = appointment;
            setTextFields();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.action_save:
                bookAppointment();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.book_appointment, menu);
    }

    public void setTextFields()
    {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setIndeterminate(true);
        dialog.setMessage("Loading appointment details...");
        dialog.setCancelable(false);
        dialog.show();

        if (appointment != null)
        {
            dialog.dismiss();
            SimpleDateFormat dateFormat = new SimpleDateFormat(ApplicationConstants.DEFAULT_DATE);
            SimpleDateFormat hourMinute = new SimpleDateFormat(ApplicationConstants.DEFAULT_TIME_12HR);
            Date startTime = new Date(appointment.getSlot().getStartTime());
            Date finishTime = new Date(appointment.getSlot().getFinishTime());

            tvDate.setText(dateFormat.format(appointment.getAppointmentDate()));
            tvStartTime.setText(hourMinute.format(startTime));
            tvFinishTime.setText(hourMinute.format(finishTime));
            tvDoctor.setText(appointment.getDoctor().toString());
            tvRoom.setText(appointment.getDoctor().getOffice());
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        outState.putSerializable("appointment", this.appointment);
    }

    public void bookAppointment()
    {
        if (ApplicationConstants.isNetworkConnected(getActivity()))
        {
            if (appointment != null)
            {
                ParseACL userACL = new ParseACL(User.getCurrentUser());
                appointment.setACL(userACL);

                final ProgressDialog signUpDialog = new ProgressDialog(getActivity());
                signUpDialog.setIndeterminate(true);
                signUpDialog.setMessage("Booking appointment. Please wait.");
                signUpDialog.show();

                appointment.saveInBackground(new SaveCallback()
                {
                    @Override
                    public void done(ParseException e)
                    {
                        signUpDialog.dismiss();
                        if (e == null)
                        {
                            Toast.makeText(getActivity(), "Appointment booked", Toast.LENGTH_SHORT).show();
                            listener.fragmentResults(getActivity().RESULT_OK);
                        } else
                        {
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            } else
            {
                Toast.makeText(getActivity(), "No appointment exists, please go back to doctor selection", Toast.LENGTH_SHORT).show();
            }
        } else
        {
            Toast.makeText(getActivity(), "Please connect to the internet to book appointment", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        try
        {
            listener = (IBookingClickListener) getActivity();
        } catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString() + " must implement IBookingClickListener");
        }
    }
}
