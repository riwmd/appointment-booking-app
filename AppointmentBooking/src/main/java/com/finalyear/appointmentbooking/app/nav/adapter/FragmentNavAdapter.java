package com.finalyear.appointmentbooking.app.nav.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.finalyear.appointmentbooking.app.ApplicationConstants;
import com.finalyear.appointmentbooking.app.R;
import com.finalyear.appointmentbooking.app.data.User;
import com.finalyear.appointmentbooking.app.nav.data.FragmentNavItem;
import com.parse.Parse;
import com.parse.ParseUser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by Natasha Whitter on 06/05/2014.
 */
public class FragmentNavAdapter extends BaseAdapter
{
    private Context context;
    private ArrayList<FragmentNavItem> items;

    public FragmentNavAdapter(Context context, ArrayList<FragmentNavItem> items)
    {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount()
    {
        return items.size();
    }

    @Override
    public Object getItem(int position)
    {
        return items.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
            if (convertView == null)
            {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.drawer_list_item, null);
            }

            ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);
            TextView txtTitle = (TextView) convertView.findViewById(R.id.title);

            imgIcon.setImageResource(items.get(position).getIcon());
            txtTitle.setText(items.get(position).getTitle());

            return convertView;
    }

    public void add(FragmentNavItem fragmentNavItem)
    {
        items.add(fragmentNavItem);
    }
}
