package com.finalyear.appointmentbooking.app.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.DatePicker;
import com.finalyear.appointmentbooking.app.R;
import com.finalyear.appointmentbooking.app.data.User;
import com.parse.ParseException;
import com.parse.SaveCallback;

import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @since 27/04/2014
 */
public class EditDateOfBirthDialog extends DialogFragment
{
    private DatePicker dpBirthDate;
    private User user;

    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_dialog_edit_dob, null);
        dpBirthDate = (DatePicker) view.findViewById(R.id.birth_date);
        user = (User) User.getCurrentUser();

        dpBirthDate.setMaxDate(Calendar.getInstance().getTimeInMillis());
        Calendar currentDate = Calendar.getInstance(TimeZone.getDefault());
        currentDate.setTime(user.getDateOfBirth());
        dpBirthDate.updateDate(currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DAY_OF_MONTH));

        dialog.setView(view);
        dialog.setTitle("Change Date of Birth");
        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int position)
            {
                saveBirthDate();
                dialog.dismiss();
            }
        });

        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                getTargetFragment().onActivityResult(getTargetRequestCode(), getActivity().RESULT_CANCELED, null);
                dialog.cancel();
            }
        });

        return dialog.create();
    }

    private void saveBirthDate()
    {
        Calendar date = Calendar.getInstance(Locale.getDefault());
        date.set(dpBirthDate.getYear(), dpBirthDate.getMonth(), dpBirthDate.getDayOfMonth(), 0, 0, 0);

        user.setDateOfBirth(date.getTime());
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setIndeterminate(true);
        dialog.setMessage("Changing date of birth. Please wait.");
        dialog.show();
        user.saveInBackground(new SaveCallback()
        {
            @Override
            public void done(ParseException e)
            {
                dialog.dismiss();
                if (e == null)
                {
                    getTargetFragment().onActivityResult(getTargetRequestCode(), getActivity().RESULT_OK, null);
                }
            }
        });
    }
}


