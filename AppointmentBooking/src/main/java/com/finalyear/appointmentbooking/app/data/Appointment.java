package com.finalyear.appointmentbooking.app.data;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.Serializable;
import java.util.Date;

/**
 * This Appointment class is an extension of ParseObject to access Appointment objects
 *
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @see com.parse.ParseObject
 * @since 08/04/2014
 */

@ParseClassName("Appointment")
public class Appointment extends ParseObject implements Serializable
{
    /**
     * Empty constructor needed for ParseObject class
     */
    public Appointment()
    {
    }

    /**
     * Gets the class which the query will use to access Appointment data.
     *
     * @return query to fetch Appointment objects
     * @see com.parse.ParseQuery
     */
    public static ParseQuery<Appointment> getQuery()
    {
        return ParseQuery.getQuery(Appointment.class);
    }

    /**
     * Gets the doctor assigned to this Appointment.
     *
     * @return this Appointment's assigned doctor
     */
    public Doctor getDoctor()
    {
        return (Doctor) getParseObject("doctor");
    }

    /**
     * Changes the doctor assigned to this Appointment
     *
     * @param doctor This Appointment's new doctor
     */
    public void setDoctor(Doctor doctor)
    {
        super.put("doctor", doctor);
    }

    /**
     * Gets the user assigned to this Appointment.
     *
     * @return this Appointment's patient
     */
    public User getPatient()
    {
        return (User) getParseUser("patient");
    }

    /**
     * Changes the user assigned to this Appointment.
     *
     * @param patient This Appointment's new patient
     */
    public void setPatient(User patient)
    {
        super.put("patient", patient);
    }

    /**
     * Gets the appointment date assigned to this Appointment.
     *
     * @return this Appointment's date
     */
    public Date getAppointmentDate()
    {
        return getDate("appointmentDate");
    }

    /**
     * Changes the appointment date assigned to this Appointment.
     *
     * @param appointmentDate This Appointment's new date
     */
    public void setAppointmentDate(Date appointmentDate)
    {
        super.put("appointmentDate", appointmentDate);
    }

    /**
     * Gets the slot assigned to this Appointment.
     *
     * @return this Appointment's slot
     */
    public Slot getSlot()
    {
        return (Slot) getParseObject("slot");
    }

    /**
     * Changes the slot assigned to this Appointment.
     *
     * @param slot This Appointment's new slot
     */
    public void setSlot(Slot slot)
    {
        super.put("slot", slot);
    }

    /**
     * Gets the status of this Appointment.
     *
     * @return this Appointment's status
     */
    public String getStatus()
    {
        return super.getString("status");
    }

    /**
     * Changes the status of this Appointment.
     *
     * @param status This Appointment's new status
     */
    public void setStatus(String status)
    {
        super.put("status", status);
    }
}
