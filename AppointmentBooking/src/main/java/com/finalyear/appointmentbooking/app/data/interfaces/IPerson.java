package com.finalyear.appointmentbooking.app.data.interfaces;

import com.parse.ParseObject;

import java.util.Date;


/**
 * This IPerson interface
 * Provides methods for classes with Person attributes
 *
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @see com.parse.ParseObject
 * @since 08/04/2014
 */
public interface IPerson
{
    /**
     * Gets the title name of this Person.
     *
     * @return this Person's title
     */
    public String getTitle();

    /**
     * Changes the title of this Person.
     *
     * @param title This Person's new title
     */
    public void setTitle(String title);

    /**
     * Gets the first name of this Person.
     *
     * @return this Person's first name
     */
    public String getFirstName();

    /**
     * Changes the first name of this Person.
     *
     * @param firstName This Person's new first name
     */
    public void setFirstName(String firstName);

    /**
     * Gets the surname of this Person.
     *
     * @return this Person's surname
     */
    public String getSurname();

    /**
     * Changes the surname of this Person.
     *
     * @param surname This Person's new surname
     */
    public void setSurname(String surname);

    /**
     * Gets the date of birth of this Person.
     *
     * @return this Person's date of birth
     */
    public Date getDateOfBirth();

    /**
     * Changes the date of birth of this Person.
     *
     * @param birthDate This Person's new date of birth
     */
    public void setDateOfBirth(Date birthDate);

    /**
     * Gets the gender of this Person.
     *
     * @return this Person's gender
     */
    public String getGender();

    /**
     * Changes the gender of this Person.
     *
     * @param gender This Person's new gender
     */
    public void setGender(String gender);

    /**
     * Gets the GP Practice of this Person.
     *
     * @return this Person's GP Practice
     */
    public ParseObject getPractice();

    /**
     * Changes the GP Practice of this Person
     *
     * @param GPPractice This Person's new GP Practice
     */
    public void setPractice(ParseObject GPPractice);
}
