package com.finalyear.appointmentbooking.app.data;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

/**
 * This class is an extension of ParseObject to access
 * information about an doctor's schedule
 *
 * @author Natasha Whitter
 * @version 1.0
 * @see com.parse.ParseObject
 * @since 08/04/2014
 */

@ParseClassName("DoctorSchedule")
public class DoctorSchedule extends ParseObject
{
    /**
     * Empty constructor needed for ParseObject class
     */
    public DoctorSchedule()
    {
    }

    /**
     * Gets the class which the query will use to access DoctorSchedule data.
     *
     * @return query to fetch DoctorSchedule objects
     * @see com.parse.ParseQuery
     */
    public static ParseQuery<DoctorSchedule> getQuery()
    {
        return ParseQuery.getQuery(DoctorSchedule.class);
    }

    /**
     * Gets the doctor assigned to the DoctorSchedule.
     *
     * @return this DoctorSchedule's assigned doctor
     */
    public ParseObject getDoctor()
    {
        return super.getParseObject("doctor");
    }

    /**
     * Changes the doctor assigned to this DoctorSchedule.
     *
     * @param doctor This DoctorSchedule's new doctor
     */
    public void setDoctor(ParseObject doctor)
    {
        super.put("doctor", doctor);
    }

    /**
     * Gets the available days of the Doctor.
     *
     * @return the Doctor's available days
     */
    public List<Integer> getDaysAvailable()
    {
        return super.getList("availableDays");
    }

    /**
     * Changes the available days of the Doctor.
     *
     * @param day The Doctor's new available day
     */
    public void setDaysAvailable(int day)
    {
        super.add("availableDays", day);
    }

    /**
     * Gets the start time of the Doctor.
     *
     * @return the Doctor's new start time
     */
    public int getStartTime()
    {
        return super.getInt("startTime");
    }

    /**
     * Changes the start time of the Doctor.
     *
     * @param startTime The Doctor's new start time
     */
    public void setStartTime(int startTime)
    {
        super.put("startTime", startTime);
    }

    /**
     * Gets the finish time of the Doctor.
     *
     * @return the Doctor's new finish time
     */
    public int getFinishTime()
    {
        return super.getInt("finishTime");
    }

    /**
     * Changes the finish time of the Doctor.
     *
     * @param finishTime The Doctor's new finish time
     */
    public void setFinishTime(int finishTime)
    {
        super.put("finishTime", finishTime);
    }
}
