package com.finalyear.appointmentbooking.app.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.finalyear.appointmentbooking.app.ApplicationConstants;
import com.finalyear.appointmentbooking.app.R;
import com.finalyear.appointmentbooking.app.data.Doctor;
import com.finalyear.appointmentbooking.app.data.Slot;
import com.parse.ParseCloud;
import com.parse.ParseException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.TimeZone;

/**
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @since 19/04/2014
 */
public class SlotAdapter extends BaseAdapter
{
    private Activity context;
    private ArrayList<Slot> objects;
    private LayoutInflater inflater;
    private Doctor doctor;
    private Calendar date;

    public SlotAdapter(Activity context)
    {
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public SlotAdapter(Activity context, Calendar appointmentDate, Doctor doctor)
    {
        this.context = context;
        this.doctor = doctor;
        this.date = appointmentDate;

        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("appointmentDate", date.getTime());
        params.put("doctor", this.doctor.getObjectId());

        try
        {
            this.objects = ParseCloud.callFunction("getAvailableSlots", params);
        } catch (ParseException e)
        {
            e.printStackTrace();
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void updateAdapter(Calendar appointmentDate, Doctor doctor)
    {
        this.date = appointmentDate;
        this.doctor = doctor;
        this.objects.clear();

        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("appointmentDate", date.getTime());
        params.put("doctor", this.doctor.getObjectId());

        try
        {
            this.objects = ParseCloud.callFunction("getAvailableSlots", params);
        } catch (ParseException e)
        {
            e.printStackTrace();
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public int getCount()
    {
        if (objects != null)
        {
            return objects.size();
        } else
        {
            return 0; // Check
        }
    }

    @Override
    public Slot getItem(int position)
    {
        if (objects != null)
        {
            return objects.get(position);
        } else
        {
            return null;
        }
    }

    @Override
    public long getItemId(int position)
    {
        if (objects != null)
        {
            return position;
        } else
        {
            return 0;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View v = convertView;

        if (v == null)
        {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.item_list_available_slots, null);
        }

        Slot slot = objects.get(position);

        if (slot != null)
        {
            Calendar startTime = Calendar.getInstance(TimeZone.getDefault());
            startTime.setTimeInMillis(slot.getStartTime());

            TextView tvStartTime = (TextView) v.findViewById(R.id.slot_start_time);

            if (tvStartTime != null)
            {
                SimpleDateFormat hourMinute = new SimpleDateFormat(ApplicationConstants.DEFAULT_TIME_12HR);
                hourMinute.setCalendar(startTime);
                tvStartTime.setText(hourMinute.format(startTime.getTime()));
            }
        }
        return v;
    }

}
