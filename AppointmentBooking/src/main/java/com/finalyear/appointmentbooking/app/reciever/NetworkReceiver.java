/**
 *
 */
package com.finalyear.appointmentbooking.app.reciever;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * @author Natasha
 */
public class NetworkReceiver extends BroadcastReceiver
{

    private static final String TAG = "NetworkReciever";
    boolean isWifiConnected = false;
    boolean isMobileConnected = false;

    /* (non-Javadoc)
     * @see android.content.BroadcastReceiver#onReceive(android.content.Context, android.content.Intent)
     */
    @Override
    public void onReceive(Context context, Intent intent)
    {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        if (activeNetwork != null && activeNetwork.isConnectedOrConnecting())
        {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
            {
                isWifiConnected = activeNetwork.isConnected();
                Log.d(TAG, "Wifi network is connected");
            }

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
            {
                isMobileConnected = activeNetwork.isConnected();
                Log.d(TAG, "Mobile network is connected");
            }

        } else
        {
            Log.d(TAG, "No Network is available");
            // Crouton.showText(, "No network is available", Style.ALERT); // find out

        }
    }

}
