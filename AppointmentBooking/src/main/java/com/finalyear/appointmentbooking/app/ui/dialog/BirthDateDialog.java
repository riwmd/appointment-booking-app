package com.finalyear.appointmentbooking.app.ui.dialog;
// http://androidtrainningcenter.blogspot.co.uk/2012/10/creating-datepicker-using.html

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import com.finalyear.appointmentbooking.app.ui.BookingActivity;

import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

public class BirthDateDialog extends DialogFragment
{
    private static String TAG = "DateDialog";

    private OnDateSetListener onDateSet;
    private Context context;
    private int year, day;
    private int month;

    public BirthDateDialog(OnDateSetListener onDateSet)
    {
        this.onDateSet = onDateSet;
    }

    public BirthDateDialog(OnDateSetListener onDateSet, Context context)
    {
        this.onDateSet = onDateSet;
    }

    public void setCallBack(OnDateSetListener ondate)
    {
        onDateSet = ondate;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        Bundle date = getArguments();
        Calendar cal = Calendar.getInstance(Locale.getDefault());
        cal.add(Calendar.DAY_OF_MONTH, 1);
        Calendar cal1 = Calendar.getInstance(Locale.getDefault());
        cal1.add(Calendar.YEAR, 1);
        final Calendar cal2 = Calendar.getInstance(Locale.getDefault());

        if (date != null)
        {
            cal2.set(Calendar.YEAR, date.getInt("year"));
            cal2.set(Calendar.MONTH, date.getInt("month"));
            cal2.set(Calendar.DAY_OF_MONTH, date.getInt("day"));
        }

        this.year = cal2.get(Calendar.YEAR);
        this.month = cal2.get(Calendar.MONTH);
        this.day = cal2.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(getActivity(), onDateSet, year, month, day);

        dialog.getDatePicker().setMaxDate(Calendar.getInstance(TimeZone.getDefault()).getTimeInMillis());
        return dialog;
    }
}
