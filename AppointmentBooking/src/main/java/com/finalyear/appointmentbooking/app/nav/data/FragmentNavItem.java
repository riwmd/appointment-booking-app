package com.finalyear.appointmentbooking.app.nav.data;

import android.os.Bundle;
import android.support.v4.app.Fragment;

/**
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @since 08/01/2014
 */
public class FragmentNavItem
{
    private Class<? extends Fragment> fragmentClass;
    private String title;
    private Bundle fragmentArgs;
    private int icon;

    public FragmentNavItem(String title, Class<? extends Fragment> fragmentClass)
    {
        this(title, fragmentClass, null);
    }

    public FragmentNavItem(String title, int icon, Class<? extends Fragment> fragmentClass)
    {
        this(title, icon, fragmentClass, null);
    }

    public FragmentNavItem(String title, Class<? extends Fragment> fragmentClass, Bundle args)
    {
        this.fragmentClass = fragmentClass;
        this.fragmentArgs = args;
        this.title = title;
    }

    public FragmentNavItem(String title, int icon, Class<? extends Fragment> fragmentClass, Bundle args)
    {
        this.fragmentClass = fragmentClass;
        this.fragmentArgs = args;
        this.title = title;
        this.icon = icon;
    }

    public Class<? extends Fragment> getFragmentClass()
    {
        return fragmentClass;
    }

    public String getTitle()
    {
        return title;
    }

    public int getIcon()
    {
        return icon;
    }

    public Bundle getFragmentArgs()
    {
        return fragmentArgs;
    }
}